FROM registry.cern.ch/acc/acc-py_cs8_ci:2021.12

RUN cat /etc/krb5.conf
RUN yum install -y epel-release
RUN yum -y update
RUN yum -y groupinstall "Development Tools"
RUN yum -y install dnf-plugins-core
RUN yum config-manager --set-enabled powertools
RUN yum -y install openssl-devel bzip2-devel libffi-devel xz-devel sqlite-devel wget which krb5-workstation krb5-libs libjpeg-turbo xrootd-client lapack-devel openblas-devel jq freetype-devel java-11-openjdk-devel

COPY krb5.conf /etc/krb5.conf

ENV JAVA_HOME=/usr/lib/jvm/java-11-openjdk
